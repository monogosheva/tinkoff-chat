//
//  ConversationCell.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 06.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell, MessageCellConfiguration {
    @IBOutlet weak var fonView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    var textMessage: String? {
        willSet {
            messageLabel.text = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fonView.layer.cornerRadius = 10
    }
}
