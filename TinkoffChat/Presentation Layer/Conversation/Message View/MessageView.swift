//
//  MessageView.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 18.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

@IBDesignable class MessageView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var messageTextFiled: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    weak var sender: MessageSendDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setting()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setting()
    }
    
    private func setting(){
        let bundle = Bundle(for: MessageView.self)
        bundle.loadNibNamed(String(describing: MessageView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    @IBAction func sendButtonClick(_ sender: UIButton) {
        self.sender?.sendButtonClick(text: messageTextFiled.text!)
        messageTextFiled.text = nil
    }
    
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
