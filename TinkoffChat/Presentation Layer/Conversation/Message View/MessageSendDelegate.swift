//
//  MessageSendDelegate.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 18.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol MessageSendDelegate: class {
    func sendButtonClick(text: String)
}
