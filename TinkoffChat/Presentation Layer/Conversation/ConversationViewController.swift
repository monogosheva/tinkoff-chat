//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 06.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit
import CoreData

class ConversationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private weak var messageView: MessageView!
    
    private let incomingMessage = String("IncomingConversationCell")
    private let outgoingMessage = String("OutgoingConversationCell")
    
    var conversationId : Int32 = 0
    
    var user: Peer? {
        didSet {
                navigationItem.title = user?.name
        }
    }

    private var service : ICommunicationService = CommunicationService()
    private var fetchRequestManager : IFetchRequestManager = FetchRequestManager<Conversation>(modelName: String(describing: Conversation.self))
    
//    private var messages : [(Message, Date, Bool)] = []
    let myPeer = Peer(identifier: "Ira's iPhone", name: "Monogosheva Irina")
    
    private var messages : [Letter] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        service.delegate = self
        fetchRequestManager.delegate = self
        editTableView()
        
    }
    
    private func gettingMessages() {
        messages = fetchRequestManager.gettingMessagesOnConversationId(convId: conversationId)
    }
    
    private func settingMessageWriter(){
        messageView.sender = self
        messageView.dropShadow(scale: true)
    }
    
    private func editTableView() {
        navigationItem.title = fetchRequestManager.gettingConversationOnId(convId: conversationId)?.relationship?.name
        tableView.register(UINib(nibName: incomingMessage, bundle: nil), forCellReuseIdentifier: incomingMessage)
        tableView.register(UINib(nibName: outgoingMessage, bundle: nil), forCellReuseIdentifier: outgoingMessage)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 34
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func updateScreen(){
        
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func generateMessageID() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX)) + \(Date.timeIntervalSinceReferenceDate) + \(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }
    
    private func showAlertErorr() {
        let alert = UIAlertController(title: "Erorr", message: "Enter your message", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ConversationViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //
        
        if user == myPeer {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: incomingMessage, for: indexPath) as? ConversationCell else {
                return UITableViewCell()
            }
            cell.textMessage = messages[indexPath.row].text
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: outgoingMessage, for: indexPath) as? ConversationCell else {
                return UITableViewCell()
            }
            cell.textMessage = messages[indexPath.row].text
            return cell
        }
    }
}

extension ConversationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ConversationViewController: CommunicationServiceDelegate {
    func updateScreen(_ communicationService: ICommunicationService) {
        //
    }
    
    func error(_ communicationService: ICommunicationService, error: String) {
        //
    }
    
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: @escaping (Bool) -> Void) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
    }
}

extension ConversationViewController: NSFetchedResultsControllerDelegate {

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        guard let indexPath = indexPath else{ return }

        switch type{
        case .update:
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else {return}
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case.insert:
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}

extension ConversationViewController: MessageSendDelegate {
    
    func sendButtonClick(text: String) {
        let message = Message(identifier: generateMessageID(), text: text, type: .textMessage)
        if let user = user {
            service.send(message, to: user)
        }
        updateScreen()
    }
    
}

