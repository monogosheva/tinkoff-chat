//
//  MessageCellConfiguration.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 06.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol MessageCellConfiguration : class {
    var textMessage : String? {get set}
}
