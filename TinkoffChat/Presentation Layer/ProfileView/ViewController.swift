//
//  ViewController.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 20.09.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var editButton: UIButton!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var nameTextView: UITextView!
    @IBOutlet private weak var aboutMeTextView: UITextView!
    @IBOutlet private weak var photoButton: UIButton!
    @IBOutlet private weak var saveStackView: UIStackView!
    @IBOutlet private weak var photoImage: UIImageView!
    @IBOutlet weak var saveInfoButton: UIButton!
    
    private var oldUser: User = User()
    private var changedUser: User = User()
    
    private var dataController: ICoreDataManager = CDManager(modelName:String(describing: Profile.self))
    
    private let idAvaSelectionView = String(describing: AvaSelectionViewController.self)
    
    private var photoURL: URL?
    
    private var wasChangePhoto = false
    private var wasChangeName = false
    private var wasChangeInfo = false
    
    private var imagePicker = UIImagePickerController()
    
    @IBAction func saveInfoAction(_ sender: UIButton) {
        self.activityIndicator.startAnimating()
        self.setNewUserData()
        dataController.save(object: changedUser){ [weak self] success in
            if success {
                DispatchQueue.main.async{ [weak self] in
                    self?.showAlertSuccessfulSave()
                    self?.wasChangePhoto = false
                    self?.wasChangeName = false
                    self?.wasChangeInfo = false
                }
                self?.readOfDB()
            } else {
                DispatchQueue.main.async {
                    self?.showAlertErrorSave()
                }
            }
        }
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        setEditMode(editMode: true)
        updateStateButton()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func photoButtonAction(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Пожалуйста выберите опцию", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Установить из галереи", style: .default) { action in
            self.openGallery()
        })
        alert.addAction(UIAlertAction(title: "Сделать фото" , style: .default) { action in
            self.openCamera()
        })
        alert.addAction(UIAlertAction(title: "Загрузить", style: .default) { action in
            self.uploadImage()
        })
        alert.addAction(UIAlertAction(title: "Отмена" , style: .cancel, handler: nil))
        self.present(alert, animated: true, completion:nil)
        
    }
    
    private func setNewUserData() {
        changedUser.name = nameTextView.text
        changedUser.profileInfo = aboutMeTextView.text
        changedUser.photo = photoImage.image
        changedUser.photoURL = photoURL
    }
    
    private func setPhotoByURL() {
        if let url = oldUser.photoURL {
            do {
                let imageData = try Data(contentsOf: url)
                DispatchQueue.main.async {
                    self.oldUser.photo = UIImage(data: imageData)
                }
            } catch {
                self.showErrorAlert(message: "Не удалось загрузить фото")
            }
        }
    }
    
    private func readOfDB() {
        dataController.read() {[weak self] user in
            if let user = user {
                self?.oldUser = user as User
                self?.setPhotoByURL()
            } else {
                self?.showErrorAlert(message: "Не удалось загрузить данные")
            }
            DispatchQueue.main.async {
//                image = UIImage(data: imageData!)
                self?.settingScreen(user: self?.oldUser)
                self?.setEditMode(editMode: false)
                self?.activityIndicator.stopAnimating()
            }
        }
    }
    
    private func settingScreen(user: User?) {
        DispatchQueue.main.async { [weak self] in
            if let user = user {
                self?.photoImage.image = user.photo
                self?.nameLabel.text = user.name
                self?.infoLabel.text = user.profileInfo
            }
        }
    }
    
    private func setEditMode(editMode: Bool) {
        nameTextView.isEditable = editMode
        aboutMeTextView.isEditable = editMode
        nameTextView.isHidden = !editMode
        aboutMeTextView.isHidden = !editMode
        nameTextView.text = nameLabel.text
        aboutMeTextView.text = infoLabel.text
        
        photoButton.isHidden = !editMode
        photoButton.isEnabled = editMode
        
        saveStackView.isHidden = !editMode
        
        editButton.isHidden = editMode
        editButton.isEnabled = !editMode
        
        infoLabel.isHidden = editMode
        nameLabel.isHidden = editMode
    }

    private func showAlertErrorSave() {
        let alert = UIAlertController(title: "Ошибка", message: "Не удалось сохранить данные", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        let refresh = UIAlertAction(title: "Refresh", style: .default) {_ in
            self.saveInfoAction(self.saveInfoButton)
        }
        alert.addAction(ok)
        alert.addAction(refresh)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertSuccessfulSave() {
        let alert = UIAlertController(title: "Данные сохранены", message: nil, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        } else {
            self.showErrorAlert(message: "Галерея не доступна")
        }
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        } else {
            self.showErrorAlert(message: "Камера не доступна")
        }
    }
    
    private func uploadImage() {
        if let vc = UIStoryboard(name: idAvaSelectionView, bundle: nil).instantiateInitialViewController() {
            self.present(vc, animated: true, completion: nil)
           
        } else { self.showErrorAlert(message: "Нет доступа к загрузке") }
    }
    
    private func showErrorAlert(message: String) {
        let al = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        al.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        self.present(al, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readOfDB()
        
        editUIElements()
        imagePicker.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let url = UserDefaults.standard.url(forKey: "photoURL") {
            do {
                let imageData = try Data(contentsOf: url)
                self.photoImage.image = UIImage(data: imageData)
            } catch { self.showErrorAlert(message: "Не удалось загрузить фото") }
        }
    }

    private func editUIElements() {
        activityIndicator.hidesWhenStopped = true
        
        aboutMeTextView.contentInsetAdjustmentBehavior = .automatic
        editButton.layer.borderColor = UIColor.black.cgColor
        editButton.layer.borderWidth = 1
        photoButton.layer.cornerRadius = photoButton.bounds.height / 2
        photoImage.layer.cornerRadius = photoButton.bounds.height / 2
        editButton.layer.cornerRadius = editButton.bounds.height / 2
        nameTextView.layer.cornerRadius = 4
        aboutMeTextView.layer.cornerRadius = 4
        
        nameTextView.textContainer.maximumNumberOfLines = 1
        nameTextView.textContainer.lineBreakMode = .byTruncatingTail
        nameTextView.delegate = self
        aboutMeTextView.delegate = self
    }
    
    private func updateStateButton(){
        saveStackView.isUserInteractionEnabled = wasChangeName ||
            wasChangePhoto || wasChangeInfo
    }
}

extension ViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let isNameTextView = textView == nameTextView
        if isNameTextView{
            wasChangeName = !oldUser.name.elementsEqual(textView.text)
        } else {
            if let profileInfo = oldUser.profileInfo {
                wasChangeInfo = !profileInfo.elementsEqual(textView.text)
            } else {
                wasChangeInfo = !textView.text.elementsEqual("")
            }
        }
        updateStateButton()
    }
}

extension ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            wasChangePhoto = true
            photoImage.image = pickedImage
            updateStateButton()
        }
        
        if let imgUrl = info[UIImagePickerControllerImageURL] as? URL {
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            let data = UIImagePNGRepresentation(image)! as NSData
            data.write(toFile: localPath!, atomically: true)
            photoURL = URL.init(fileURLWithPath: localPath!)
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}

