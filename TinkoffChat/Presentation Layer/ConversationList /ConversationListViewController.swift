//
//  ConversationListViewController.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 03.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit
import CoreData

class ConversationListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func goToProfileAction(_ sender: Any) {
        if let vc = UIStoryboard(name: idProfile, bundle: nil).instantiateInitialViewController() {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private var service : ICommunicationService = CommunicationService()
    private var fetchRequestManager : IFetchRequestManager = FetchRequestManager<Conversation>(modelName: String(describing: Conversation.self))
    
    private let identifier = String(describing: ConversationListTableViewCell.self)
    private let idConversationView = String(describing: ConversationViewController.self)
    private let idProfile = String(describing: ViewController.self)
    
    //private var headers : [String] = ["Online", "History"]
    private var headers : [String] = ["Online"]
    
    private var dialogues : [Conversation] = []
    
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: {_,_ in })
        return container
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editTableView()
        
        service.delegate = self
        service.online = true
        fetchRequestManager.delegate = self
    }
    
    private func editTableView() {
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func updateListUser() {
        dialogues = fetchRequestManager.gettingConversationWhichTheUserIsOnline()
        tableView.reloadData()
    }
    
    private func showAlertInvite(name: String, handler: @escaping (Bool)->() ) {
        let alert = UIAlertController(title: "Invite", message: "communicate with \(name)?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: {_ in handler(true)})
        let no = UIAlertAction(title: "No", style: .cancel, handler: {_ in handler(false)})
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertError(text: String) {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ConversationListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return headers.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ConversationListTableViewCell else {
                return UITableViewCell()
            }

        let model = dialogues[indexPath.row]
        
        cell.name = model.relationship?.name
        cell.date = model.date as Date?
        cell.message = model.relationship1?.text
        cell.online = true
        cell.hasUnreadMessages = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headers[section]
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
}

extension ConversationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //if service.connect(peer: onlinePeers[indexPath.row]) {
            tableView.deselectRow(at: indexPath, animated: false)
            
            if let vc = UIStoryboard(name: "Conversation", bundle: nil).instantiateViewController(withIdentifier: idConversationView ) as? ConversationViewController {
                
                vc.conversationId = dialogues[indexPath.row].conversationId
                navigationController?.pushViewController(vc, animated: true)
            }
        //} else {
        //    service.sendInvite(peer: onlinePeers[indexPath.row])
        //    }
        }
}

extension ConversationListViewController: CommunicationServiceDelegate {
    func updateScreen(_ communicationService: ICommunicationService) {
        updateListUser()
    }
    
    func error(_ communicationService: ICommunicationService, error: String) {
        showAlertError(text: "")
    }
    
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        updateListUser()
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        updateListUser()
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
        showAlertError(text: "Did not start browsing for peers")
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: @escaping (Bool) -> Void) {
        print("didReceiveInvite")
        showAlertInvite(name: peer.identifier, handler: invintationClosure)
        
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
        showAlertError(text: "Did not start advertising for peers")
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
        updateListUser()
    }
}

extension ConversationListViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        guard let indexPath = indexPath else{ return }
        
        switch type{
        case .update:
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let newIndexPath = newIndexPath else {return}
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case.insert:
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}





