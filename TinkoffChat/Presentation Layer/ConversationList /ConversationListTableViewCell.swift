//
//  ConversationListTableViewCell.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 03.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

class ConversationListTableViewCell: UITableViewCell, ConversationCellConfiguration {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLable: UILabel!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    
    var name: String? {
        willSet {
            nameLabel.text = newValue
        }
    }
    
    var message: String? {
        willSet {
            if newValue == nil {
                messageLable.text = "No messages yet"
                messageLable.font = UIFont.italicSystemFont(ofSize: messageLable.font.pointSize)
            } else {
                messageLable.text = newValue
                messageLable.font = UIFont.systemFont(ofSize: messageLable.font.pointSize, weight: UIFont.Weight.regular)
            }
        }
    }
    
    var date: Date? {
        willSet {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = formatForDate(date: newValue)
            dateFormatterGet.locale = Locale(identifier: "RU_ru")
            if let newVal = newValue {
                timeLable.text = dateFormatterGet.string(from: newVal)
            } else {
                timeLable.text = ""
            }
        }
    }
    
    private func formatForDate(date : Date?) -> String? {
        if let date = date {
            return isSameDate(date: date) ? "HH:mm" : "dd MMM"
        }
        return nil
    }
    
    private func isSameDate(date : Date) -> Bool {
        let nowDate = Date()
        return Calendar.current.compare(nowDate, to: date, toGranularity: .day).rawValue == 0 && Calendar.current.compare(nowDate, to: date, toGranularity: .month).rawValue == 0 && Calendar.current.compare(nowDate, to: date, toGranularity: .year).rawValue == 0
    }
    
    var online: Bool = true {
        didSet {
            backgroundColor = online ? UIColor.init(red: 255.0/255.0, green: 241.0/255.0, blue: 167.0/255.0, alpha: 1.0) : UIColor.white
        }
    }
    
    var hasUnreadMessages: Bool = true {
        didSet {
            if message != nil {
                messageLable.font = hasUnreadMessages ? UIFont.systemFont(ofSize: messageLable.font.pointSize, weight: UIFont.Weight.bold) : UIFont.systemFont(ofSize: messageLable.font.pointSize, weight: UIFont.Weight.regular)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        photoImageView.layer.cornerRadius = photoImageView.bounds.height / 2
    }
}
