//
//  CollectionViewCell.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        imageView.image = nil
    }
    
}
