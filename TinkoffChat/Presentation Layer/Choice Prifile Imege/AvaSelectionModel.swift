//
//  AvaSelectionModel.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

struct CellDisplayModel {
    let imageUrl: String
}

protocol IAvaSelectionModel: class {
    var delegate: IAvaSelectionModelDelegate? { get set }
    func fetchTopImage()
}

protocol IAvaSelectionModelDelegate: class {
    func setup(dataSource: [CellDisplayModel])
    func show(error message: String)
}

class AvaSelectionModel: IAvaSelectionModel {
    weak var delegate: IAvaSelectionModelDelegate?
    
    let imageService: INetworkService = NetworkService()
    
    func fetchTopImage() {
        imageService.loadTopImages {(images: [PixabayModel]?, errorMessage) in
            
            if let images = images {
                let cells = images.map({ CellDisplayModel( imageUrl: $0.webformatURL) })
                self.delegate?.setup(dataSource: cells)
            } else {
                self.delegate?.show(error: errorMessage ?? "Error")
            }
        }
        
    }
    
    
}
