//
//  AvaSelectionViewController.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit

class AvaSelectionViewController: UIViewController {
    
    //UI
    @IBOutlet weak var collectonView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // Dependencies
    
    private let model: IAvaSelectionModel = AvaSelectionModel()
    
    // DeisplayModel
    private var dataSource: [CellDisplayModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        configureCollectionView()
        model.fetchTopImage()
        model.delegate = self
    }
    
    // MARK: - Private methods
    
    private func configureCollectionView() {
        let width = (view.frame.size.width - 20)/3
        let layout = collectonView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
        
        collectonView.delegate = self
        collectonView.dataSource = self
    }
}

extension AvaSelectionViewController: IAvaSelectionModelDelegate {
    func setup(dataSource: [CellDisplayModel]) {
        self.dataSource = dataSource
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.collectonView.reloadData()
        }
    }
    
    func show(error message: String) {
        print(message)
    }
    
}

extension AvaSelectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        let url = URL(string: self.dataSource[indexPath.row].imageUrl)
        UserDefaults.standard.set(url, forKey: "photoURL")
        
        self.dismiss(animated: true, completion: nil)
        
    }
}

extension AvaSelectionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionViewCell else {
            return UICollectionViewCell()
        }
        
        if let url = URL(string: dataSource[indexPath.row].imageUrl) {
            let data = try? Data(contentsOf: url)
        
            cell.imageView.image = UIImage(data: data!)
        }
        return cell
    }
    
}
