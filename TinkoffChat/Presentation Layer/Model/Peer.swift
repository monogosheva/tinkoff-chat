//
//  Peer.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 27.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

struct Peer: Hashable {
    let identifier: String
    let name: String
    
    var hashValue: Int{
        return identifier.hashValue
    }
    
    static func ==(lhs: Peer, rhs: Peer) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
