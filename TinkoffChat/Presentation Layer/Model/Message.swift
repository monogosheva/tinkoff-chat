//
//  Message.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 07.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

//struct Message {
//    var user : User?
//    var messageText : String?
//}

struct Message: Codable {
    enum `Type`: String {
        case textMessage = "TextMessage"
    }

    let identifier: String
    let text: String
    let type: Type
    
    enum CodingKeys: String, CodingKey {
        case eventType
        case messageId
        case text
    }
    
    func encode(to encoder: Encoder) throws { // Кодируем
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(type.rawValue, forKey: .eventType)
        try container.encode(identifier, forKey: .messageId)
        try container.encode(text, forKey: .text)
    }
    
    init(identifier: String, text: String, type: Type) {
        self.identifier = identifier;
        self.text = text
        self.type = type
    }
    
    init(identifier: String, text: String) {
        self.init(identifier: identifier, text: text, type: .textMessage)
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try container.decode(String.self, forKey: .messageId)
        text = try container.decode(String.self, forKey: .text)        
        let eventType = try container.decode(String.self, forKey: .eventType)
        type = Message.getType(eventType: eventType)
    }
    
    private static func getType(eventType: String) -> Type {
        switch eventType {
        case Type.textMessage.rawValue:
            return Type.textMessage
        default:
            return Type.textMessage
        }
    }
    
    
}
