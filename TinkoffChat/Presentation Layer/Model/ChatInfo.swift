//
//  ChatInfo.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 06.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

struct ChatInfo {
    var user : User?
    var message : String?
    var date : NSDate?
    var hasUnreadMessages : Bool = false
}
