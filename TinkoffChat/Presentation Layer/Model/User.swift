//
//  User.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 06.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import UIKit

class User: NSObject, NSCoding {
    
    var name : String
    var profileInfo : String?
    var photo : UIImage?
    var online : Bool = false
    var photoURL : URL?
    
    struct PropertyKey {
        static let name = "name"
        static let info = "info"
        static let photo = "photo"
    }
    
    //MARK: Archiving Paths
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("user")
    
    init(name: String, profileInfo: String?, photo: UIImage?, online: Bool, photoURL: URL?) {
        self.name = name
        self.profileInfo = profileInfo
        self.photo = photo
        self.online = online
        self.photoURL = photoURL
    }
    
    convenience override init() {
        self.init(name: "", profileInfo: nil, photo: nil, online: false, photoURL: nil)
    }
    
    convenience init(name: String, online: Bool) {
        self.init(name: name, profileInfo: nil, photo: nil, online: online, photoURL: nil)
    }
    
    convenience init(name: String, profileInfo: String?, photo: UIImage?) {
        self.init(name: name, profileInfo: profileInfo, photo: photo, online: false, photoURL: nil)
    }
    
    convenience init(name: String, profileInfo: String?, photoURL: URL?) {
        self.init(name: name, profileInfo: profileInfo, photo: nil, online: false, photoURL: photoURL)
    }
    
    //MARK: NSCoding
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(profileInfo, forKey: PropertyKey.info)
        aCoder.encode(photo, forKey: PropertyKey.photo)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            return nil
        }
        
        let profileInfo = aDecoder.decodeObject(forKey: PropertyKey.info) as? String
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage
        
        self.init(name: name, profileInfo: profileInfo, photo: photo)
    }
}

