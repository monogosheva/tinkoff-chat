//
//  NetworkService.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol INetworkService {
    func loadTopImages(completionHandler: @escaping ([PixabayModel]?, String?) -> Void)
}

class NetworkService: INetworkService {
    
    let requestSender: IRequestSender = RequestSender()
    
    func loadTopImages(completionHandler: @escaping ([PixabayModel]?, String?) -> Void) {
        let requestConfig = RequestsFactory.LastPixabayRequests.topImageConfig()
        
        requestSender.send(requestConfig: requestConfig) { (result: Result<[PixabayModel]>) in
            switch result {
            case .success(let images):
                completionHandler(images, nil)
            case .error(let error):
                completionHandler(nil, error)
            }
        }
        
    }
}
