//
//  BuilderFetchRequestProtocol.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import CoreData

protocol BuilderFetchRequestProtocol {
    
    func setEntityName(name: String)
    func setSortDescriptors(sortDescriptors: [NSSortDescriptor])
    func setSortDescriptor(sortDescriptor: NSSortDescriptor)
    func setPredicate(predicate: NSPredicate)
    func setResultType(resaltType: NSFetchRequestResultType)
//    func build() -> NSFetchRequest<NSFetchRequestResult>?
}
