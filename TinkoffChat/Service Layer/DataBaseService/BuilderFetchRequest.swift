//
//  BuilderFetchRequest.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import CoreData

class BuilderFetchRequest<T: NSFetchRequestResult>: BuilderFetchRequestProtocol {

    private var entityName: String? = nil
    private var predicate: NSPredicate? = nil
    private var sortDescriptor: [NSSortDescriptor] = []
    //private var fetchRequestResult: NSFetchRequestResult? = nil
    private var fetchRequestResultType: NSFetchRequestResultType? = nil
    
    func setEntityName(name: String) {
        self.entityName = name
    }
    
    func setSortDescriptor(sortDescriptor: NSSortDescriptor) {
        self.sortDescriptor.append(sortDescriptor)
    }
    
    func setSortDescriptors(sortDescriptors: [NSSortDescriptor]) {
        self.sortDescriptor += sortDescriptor
    }
    
    func setPredicate(predicate: NSPredicate) {
        self.predicate = predicate
    }

    func setResultType(resaltType: NSFetchRequestResultType) {
        self.fetchRequestResultType = resaltType
    }
    
    func build() -> NSFetchRequest<T>? {
        if let entityName = self.entityName, let resultType = self.fetchRequestResultType {
            let fetch = NSFetchRequest<T>(entityName: String(describing: entityName.self))
            if let predicate = predicate {
                fetch.predicate = predicate
            }
            fetch.sortDescriptors = sortDescriptor
            fetch.resultType = resultType
            return fetch
        }
        assertionFailure("Not all field fil")
        return nil
    }
}
