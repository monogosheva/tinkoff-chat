//
//  IFetchRequestManager.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import CoreData

protocol IFetchRequestManager {
    var delegate: NSFetchedResultsControllerDelegate? { get set }
    func gettingConversationOnId(convId: Int32) -> Conversation?
    func gettingConversationWhichTheUserIsOnline() -> [Conversation]
    func gettingMessagesOnConversationId(convId: Int32) -> [Letter]
    func gettingOnlineUsers() -> [Profile]
    func gettingUserOnId(userId: Int32) -> Profile
}
