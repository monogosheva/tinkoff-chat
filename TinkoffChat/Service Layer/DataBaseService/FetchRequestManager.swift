//
//  FetchRequestManager.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 11.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import CoreData

class FetchRequestManager<T: NSFetchRequestResult>: IFetchRequestManager {

    private let modelName: String
    weak var delegate: NSFetchedResultsControllerDelegate?

    init(modelName: String) {
        self.modelName = modelName
    }
    
    private lazy var dataController: ICoreDataManager = CDManager(modelName: String(describing: modelName.self))
    
    private lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: String(describing: modelName.self))
        container.loadPersistentStores { (_, error) in
            if let error = error {
                fatalError("Failed to load store: \(error)")
            }
        }
        return container
    }()
    
    lazy var fetchResultsController: NSFetchedResultsController<T>? = {
        var sortDescriptor : [NSSortDescriptor] = []
        switch modelName {
        case String(describing: Conversation.self):
            sortDescriptor.append(NSSortDescriptor(key: "date", ascending: true))
        case String(describing: Profile.self):
            sortDescriptor.append(NSSortDescriptor(key: "name", ascending: true))
        case String(describing: Letter.self):
            sortDescriptor.append(NSSortDescriptor(key: "userId", ascending: true))
        default:
            print("Error!")
        }
        
        let request = NSFetchRequest<T>(entityName: modelName)
        request.sortDescriptors = sortDescriptor
        request.resultType = .managedObjectResultType

        let controller = NSFetchedResultsController(fetchRequest: request,
                                                    managedObjectContext: container.viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)
        
        controller.delegate = delegate
        return controller
    }()
    
    //Шаблон 1
    //получение беседы (Conversation) с определенным conversationId
    
    func gettingConversationOnId(convId: Int32) -> Conversation? {
        var conversation : Conversation? = nil
        
        let predicate = NSPredicate(format: "conversationId == %@", convId)
        let requst = BuilderFetchRequest<T>()
        requst.setPredicate(predicate: predicate)
        requst.setEntityName(name: String(describing: Conversation.self))
        if let fetchRequest = requst.build() {
            let context = container.viewContext
            context.performAndWait {
                do {
                    let result = try context.fetch(fetchRequest)
                    conversation = result.last as? Conversation
                } catch {
                    print("Не удалось получить сообщения")
                }
            }
            return conversation
        }
        return nil
    }
    
    //Шаблон 2
    //получение непустых бесед, в которых пользователь (User) онлайн
    
    func gettingConversationWhichTheUserIsOnline() -> [Conversation] {
        var dialogues: [Conversation] = []
        
        let predicate = NSPredicate(format: "online == true")
        let fetchRequest = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
        fetchRequest.predicate = predicate
        
        let context = container.viewContext
        context.performAndWait {
            let users = try? context.fetch(fetchRequest) // получили users которые онлайн
            for user in users! {
                if let conversation = user.relationship {
                    if conversation.relationship1 != nil { //проверка, что есть последнее собщение
                        dialogues.append(conversation)
                    }
                }
            }
        }
        return dialogues
    }
    
    //Шаблон 3
    //получение сообщений (Message) из определенной беседы по id беседы
    
    func gettingMessagesOnConversationId(convId: Int32) -> [Letter] {
        var letters: [Letter] = []
        
        let predicate = NSPredicate(format: "conversationId == %@", convId)
        let fetchRequest = NSFetchRequest<Letter>(entityName: String(describing: Letter.self))
        fetchRequest.predicate = predicate
        
        let context = container.viewContext
        context.performAndWait {
            do {
                letters = try context.fetch(fetchRequest)
            } catch {
                print("Не удалось получить сообщения")
            }
        }
        return letters
    }
    
    //Шаблон 4
    //получение пользователей онлайн
    
    func gettingOnlineUsers() -> [Profile] {
        var users : [Profile] = []
        
        let predicate = NSPredicate(format: "online == %@", true)
        let fetchRequest = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
        fetchRequest.predicate = predicate
        
        let context = container.viewContext
        context.performAndWait {
            do {
                users = try context.fetch(fetchRequest)
            } catch {
                print("Не удалось получить сообщения")
            }
        }
        return users
    }
    
    //Шаблон 5
    //получение пользователя с определенным userId
    
    func gettingUserOnId(userId: Int32) -> Profile {
        var user = Profile()
        
        let predicate = NSPredicate(format: "userId == %@", userId)
        let fetchRequest = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
        fetchRequest.predicate = predicate
        
        let context = container.viewContext
        context.performAndWait {
            do {
                let result = try context.fetch(fetchRequest)
                user = result.last!
            } catch {
                print("Не удалось получить сообщения")
            }
        }
        return user
    }
}




