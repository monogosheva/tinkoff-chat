//
//  CommunicationService.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 18.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import UIKit
import Foundation
import MultipeerConnectivity

class CommunicationService: NSObject, ICommunicationService {
    
    weak var delegate: CommunicationServiceDelegate?
    
    private var DBManager : ICoreDataManager = CDManager(modelName: "Conversation")
    private var communcationManager: ICommunicationManagerProtocol = CommunicationManager()
    var online: Bool = false {
        willSet {
            if online == newValue {
                return
            } else {
                communcationManager.setOnline(online: newValue)
                communcationManager.setBrowsing(browsing: newValue)
            }
        }
    }
    
    func send(_ message: Message, to peer: Peer) {
        communcationManager.send(message: message, to: peer)
    }
    
    override init() {
        super.init()
        communcationManager.delegate = self
    }
    
    func sendInvite(peer: Peer) {
        communcationManager.sendInvite(to: peer)
    }
}

extension CommunicationService: CommunicationManagerDelegate{
    func receivedInvite(from peer: Peer, handler: @escaping (Bool) -> ()) {
        delegate?.communicationService(self, didReceiveInviteFromPeer: peer) { answer in
            handler(answer)
        }
    }
    
    func received(message: Message, from peer: Peer) {
        // записать сообщение в бд
        delegate?.communicationService(self, didReceiveMessage: message, from: peer)
    }
    
    func messageSended(message: Message) {
        // записать сообщение в бд
        delegate?.updateScreen(self)
    }
    
    func error(error: String) {
        delegate?.error(self, error: error)
    }
    
    func lost(peer: Peer) {
        // удалить из бд
        delegate?.updateScreen(self)
    }
    
    func find(peer: Peer) {
        // добавить в бд
        delegate?.updateScreen(self)
        
    }
    
}
