//
//  CommunicationServiceDelegate.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 27.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol CommunicationServiceDelegate: class {
    /// Browsing
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer)
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer)
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error)
    /// Advertising
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: @escaping (Bool) -> Void)
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error)
    /// Messages
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer)
    
    func updateScreen(_ communicationService: ICommunicationService)

    func error(_ communicationService: ICommunicationService, error: String)
}
