//
//  ICommunicationService.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 27.10.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    var delegate: CommunicationServiceDelegate? { get set }
    var online: Bool { get set }
    func send(_ message: Message, to peer: Peer)
}
