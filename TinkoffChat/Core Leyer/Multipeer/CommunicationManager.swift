//
//  CommunicationManager.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CommunicationManager: NSObject, ICommunicationManagerProtocol {

    static var mcPeerIDs: [Peer : MCPeerID] = [:]
    
    static let maxTimeWaiting: Double = 20000
    
    private let myPeer = Peer(identifier: "Ira's iPhone", name: "Monogosheva Irina")
    private let chatServiceType = "tinkoff-chat"
    private let myPeerID : MCPeerID
    private let advertiser : MCNearbyServiceAdvertiser
    private let session : MCSession
    private let browser : MCNearbyServiceBrowser
    var delegate: CommunicationManagerDelegate?

    override init(){
        myPeerID = MCPeerID(displayName: myPeer.identifier)
        advertiser = MCNearbyServiceAdvertiser(peer: myPeerID, discoveryInfo: ["userName" : myPeer.name], serviceType: chatServiceType)
        session = MCSession(peer: myPeerID)
        browser = MCNearbyServiceBrowser(peer: myPeerID, serviceType: chatServiceType)
        super.init()
        advertiser.delegate = self
        session.delegate = self
        browser.delegate = self

    }
    
    func setOnline(online: Bool) {
        if online {
            advertiser.startAdvertisingPeer()
        }else{
            advertiser.stopAdvertisingPeer()
        }
    }
    
    func setBrowsing(browsing: Bool) {
        if browsing {
            browser.startBrowsingForPeers()
        }else{
            browser.stopBrowsingForPeers()
        }
    }
    
    func send(message: Message, to peer: Peer) {
        let peerID = CommunicationManager.mcPeerIDs[peer]
        let encodedMessage = try? JSONEncoder().encode(message)
        if let peerID = peerID, let message = encodedMessage{
            try? session.send(message, toPeers: [peerID], with: MCSessionSendDataMode.reliable) // Сообщения в date
        }else{
            delegate?.error(error: "error")
        }
    }
    
    func sendInvite(to peer: Peer) {
        let peerID = CommunicationManager.mcPeerIDs[peer]
        if let peerID = peerID{
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: CommunicationManager.maxTimeWaiting)
        }else{
            delegate?.error(error: "error")
        }
    }
    
}

extension CommunicationManager: MCNearbyServiceAdvertiserDelegate {
    //получил инвайт
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        //проверка, что уже не подключились
        for conectedPeer in session.connectedPeers {
            if conectedPeer == peerID { // уже подключились
                return
            }
        }
        let peer = Peer(identifier: peerID.displayName, name: "")
        
        delegate?.receivedInvite(from: peer){ [weak self] answer in
            invitationHandler(answer, self?.session)
        }
        print("didReceiveInvitationFromPeer")
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        delegate?.error(error: "error")
    }
}

extension CommunicationManager: MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        if let name = info?["userName"] {
            let peer = Peer(identifier: peerID.displayName, name: name)
            CommunicationManager.mcPeerIDs[peer] = peerID
            delegate?.find(peer: peer)
        }
        print("found peer \(peerID)")
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        let peer = Peer(identifier: peerID.displayName, name: "")
        delegate?.lost(peer: peer)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        delegate?.error(error: "error")
    }
}

extension CommunicationManager: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) { //Получил сообщение
        let message = try! JSONDecoder().decode(Message.self, from: data)
        let peer = Peer(identifier: peerID.displayName, name: "")
        delegate?.received(message: message, from: peer)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }
}



