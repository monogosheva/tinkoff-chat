//
//  ICommunicationManagerProtocol.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import MultipeerConnectivity


protocol ICommunicationManagerProtocol {
    
    static var mcPeerIDs: [Peer : MCPeerID] { get }
    var delegate: CommunicationManagerDelegate? { get set }
    func setOnline(online: Bool)
    func setBrowsing(browsing: Bool)
    func send(message: Message, to peer: Peer)
    func sendInvite(to peer: Peer)
    
}
