//
//  CommunicationManagerDelegate.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 17.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation


protocol CommunicationManagerDelegate{
    
    func receivedInvite(from peer:Peer, handler: @escaping (Bool) -> ())
    func received(message: Message, from peer: Peer)
    func messageSended(message: Message)
    func error(error: String)
    func lost(peer: Peer)
    func find(peer: Peer)
    
}
