//
//  RequestsFactory.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

struct RequestsFactory {
    struct LastPixabayRequests {
        static func topImageConfig() -> RequestConfig<PixabayParser> {
            let request = PixabayRequest(apiKey: "10790533-467cb0449dfa57beb7f6d08a1")
            return RequestConfig<PixabayParser>(request:request, parser: PixabayParser())
        }
    }
}
