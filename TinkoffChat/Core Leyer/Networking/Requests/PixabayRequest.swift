//
//  AppleRSSRequest.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

// https//pixabay.com/api/?key={ВАШ_АПИ_КЛЮЧ}&q=yellow+flowers&image_type=photo&pretty=true&per_page=21

class PixabayRequest: IRequest {
    private let apiKey: String
    private var urlStringEnd: String = "&q=yellow+flowers&image_type=photo&pretty=true&per_page=100"
    private var urlStringStart: String =  "https://pixabay.com/api/?key="
    
    // MARK: - IRequest
    
    var urlRequest: URLRequest? {
        let urlString = urlStringStart + apiKey + urlStringEnd
        if let url = URL(string: urlString) {
            return URLRequest(url: url)
        }
        return nil
    }
    
    // MARK: - Initialization
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
}

