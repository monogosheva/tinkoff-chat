//
//  AppsParser.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 24.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

struct PixabayResponse: Codable {
    let hits: [PixabayModel]
}

struct PixabayModel: Codable {
    let id: Int
    let webformatURL: String
}

class PixabayParser: IParser {
    typealias Model = [PixabayModel]
    
    func parse(data: Data) -> [PixabayModel]? {
        do {
            let response = try JSONDecoder().decode(PixabayResponse.self, from: data)
            return response.hits
        } catch  {
            print("error trying to convert data to JSON")
            return nil
        }
    }
}
