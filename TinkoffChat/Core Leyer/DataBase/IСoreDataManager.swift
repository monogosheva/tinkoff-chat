//
//  СoreDataManager.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 04.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation

protocol ICoreDataManager {
    func save(object: Any, handler: @escaping (Bool)->())
    func read(handler: @escaping (User?)->())
}
