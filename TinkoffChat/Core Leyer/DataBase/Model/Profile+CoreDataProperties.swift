//
//  Profile+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 11.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var photo: URL?
    @NSManaged public var profileInfo: String?
    @NSManaged public var profileId: Int32
    @NSManaged public var online: Bool
    @NSManaged public var relationship: Conversation?
    @NSManaged public var relationship1: Letter?

}
