//
//  Conversation+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 11.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//
//

import Foundation
import CoreData


extension Conversation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Conversation> {
        return NSFetchRequest<Conversation>(entityName: "Conversation")
    }

    @NSManaged public var conversationId: Int32
    @NSManaged public var userId: Int32
    @NSManaged public var lastMessageId: Int32
    @NSManaged public var date: NSDate?
    @NSManaged public var relationship: Profile?
    @NSManaged public var relationship1: Letter?

}
