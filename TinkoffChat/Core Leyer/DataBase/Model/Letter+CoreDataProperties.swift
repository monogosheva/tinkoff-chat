//
//  Letter+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 11.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//
//

import Foundation
import CoreData


extension Letter {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Letter> {
        return NSFetchRequest<Letter>(entityName: "Letter")
    }

    @NSManaged public var messageId: Int32
    @NSManaged public var text: String?
    @NSManaged public var type: String?
    @NSManaged public var conversationId: Int32
    @NSManaged public var userId: Int32
    @NSManaged public var relationship: Profile?
    @NSManaged public var relationship1: Conversation?

}
