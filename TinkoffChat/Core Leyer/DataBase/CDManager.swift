//
//  Storage.swift
//  TinkoffChat
//
//  Created by Ирина Моногошева on 03.11.2018.
//  Copyright © 2018 Ирина Моногошева. All rights reserved.
//

import Foundation
import CoreData

class CDManager: ICoreDataManager {

    private let modelName: String
    
    init(modelName: String) {
        self.modelName = modelName
    }
    
    private lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name:  modelName)
        container.loadPersistentStores { (_, error) in
            if let error = error {
                fatalError("Failed to load store: \(error)")
            }
        }
        return container
    }()

    func save(object: Any, handler: @escaping (Bool)->()) {
        let context = container.viewContext
        context.performAndWait {
            _ = self.defineEntity(object: object, context: context)
            do {
                try context.save()
                handler(true)
            } catch {
                handler(false)
            }
        }
    }
    
    func defineEntity(object: Any, context: NSManagedObjectContext) -> NSManagedObject? {

        if let user = object as? User {
            let profile = NSEntityDescription.insertNewObject(forEntityName: String(describing: Profile.self), into: context) as? Profile

            profile?.name = user.name
            profile?.profileInfo = user.profileInfo
            profile?.photo = user.photoURL
            return profile
        } else {
            if let chatInfo = object as? ChatInfo {
                let conversation = NSEntityDescription.insertNewObject(forEntityName: String(describing: Conversation.self), into: context) as? Conversation
                
                conversation?.date = chatInfo.date as NSDate?
                return conversation
            } else {
                if let message = object as? Message {
                    let letter = NSEntityDescription.insertNewObject(forEntityName: String(describing: Letter.self), into: context) as? Letter
                    letter?.text = message.text
                    letter?.type = "TextMessage"
                    return letter
                }
            }
        }
        return nil
    }
    
    func read(handler: @escaping (User?)->()) {
        let context = container.viewContext
        context.performAndWait {
            var user: User?

            let requst = NSFetchRequest<Profile>(entityName: String(describing: Profile.self))
            let result = try? context.fetch(requst)
            let profile = result?.last

            if let name = profile?.name {
                user = User(name: name, profileInfo: profile?.profileInfo, photoURL: profile?.photo)
            }
            handler(user)
        }
    }
}
