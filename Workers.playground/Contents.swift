//: Playground - noun: a place where people can play

import UIKit

class CEO {
    var name = "Гомер Симпсон"
    var manager : ProductManager?
    
    lazy var printManager = { [weak self] in
        print("Product Manager: \(self?.manager?.name ?? "неопределенн")")
    }
    
    lazy var showCompany = { [weak self] in
        self?.manager?.printCompany()
        
    }
    
    lazy var printDevelopers = { [weak self] in
        print("Developer: \(self?.manager?.developer1?.name ?? "неопределенн")")
        print("Developer: \(self?.manager?.developer2?.name ?? "неопределенн")")
    }
    
    deinit {
        print("CEO deinit")
    }
}

class ProductManager {
    var name = "Мардж Симпсон"
    weak var ceo : CEO?
    var developer1 : Developer?
    var developer2 : Developer?
    
    func printCompany() {
        print("CEO: \(ceo?.name ?? "неопределенн")")
        print("Product Manager: \(name)")
        print("Developer: \(developer1?.name ?? "неопределенн")")
        print("Developer: \(developer2?.name ?? "неопределенн")")
    }
    
    deinit {
        print("ProductManager deinit")
    }
}

class Developer {
    var name : String?
    weak var manager : ProductManager?
    init(name : String) {
        self.name = name
    }
    
    func communicateWithTheDevelopers(message : String) {
        if manager?.developer1 !== self {
            print("\(manager?.developer1?.name ?? "Нет разработчика") " + message)
        } else {
            print("\(manager?.developer2?.name ?? "Нет разработчика") " + message)
        }
    }
    
    func communicateWithTheManager(message : String) {
        print("\(manager?.name ?? "Нет менеджера") " + message)
    }
    
    func communicateWithTheSeo(message : String) {
        print("\(manager?.ceo?.name ?? "Нет сео") " + message)
    }
    
    deinit {
        print("Developer deinit")
    }
}

class Company {
    var ceo : CEO?
    var manager : ProductManager?
    var developer1 : Developer?
    var developer2 : Developer?
    
    func communication() {
        ceo?.showCompany()
        developer2?.communicateWithTheManager(message: "Как дела?")
        ceo?.printDevelopers()
        ceo?.printManager()
        developer1?.communicateWithTheDevelopers(message: "Привет!")
        developer1?.communicateWithTheSeo(message: "Что делаешь?")
    }
    deinit {
        print("Company deinit")
    }
}

func createCompany() -> Company {
    let company = Company()
    company.ceo = CEO()
    company.manager = ProductManager()
    company.developer1 = Developer(name : "Бард Симпсон")
    company.developer2 = Developer(name : "Лиза Симпсон")
    
    company.ceo?.manager = company.manager
    company.manager?.ceo = company.ceo
    company.manager?.developer1 = company.developer1
    company.manager?.developer2 = company.developer2
    company.developer1?.manager = company.manager
    company.developer2?.manager = company.manager
    
    return company
}

func main(){
    let c = createCompany()
    c.communication()
    c.ceo?.printManager()
}

main()
